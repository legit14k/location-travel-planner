﻿/// <summary>
/// Jason Grimberg
/// CE06 TreeView and TabControl
/// Data class to return the direction string
/// </summary>
/// 
namespace JasonGrimberg_CE06
{
    public class Data
    {
        // Variables for all text
        string direction;
        decimal miles;
        decimal hours;
        string mode;

        // Constructor for direction
        public string Direction
        {
            // Getters and setters for direction
            get { return direction; }
            set { direction = value; }
        }

        // Constructor for miles
        public decimal Miles
        {
            // Getters and setters for miles
            get { return miles; }
            set { miles = value; }
        }

        // Constructors for hours
        public decimal Hours
        {
            // Getters and setters for hours
            get { return hours; }
            set { hours = value; }
        }

        // Constructors for mode
        public string Mode
        {
            // Getters and setters for mode
            get { return mode; }
            set { mode = value; }
        }

        // To-string override
        public override string ToString()
        {
            // Return the direction string
            return Direction.ToString();
        }

    }
}
