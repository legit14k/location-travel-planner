﻿namespace JasonGrimberg_CE06
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.menuFile = new System.Windows.Forms.ToolStripMenuItem();
            this.menuNew = new System.Windows.Forms.ToolStripMenuItem();
            this.menuExit = new System.Windows.Forms.ToolStripMenuItem();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.legTab = new System.Windows.Forms.TabPage();
            this.btnAdd = new System.Windows.Forms.Button();
            this.gbLeg = new System.Windows.Forms.GroupBox();
            this.tbMode = new System.Windows.Forms.TextBox();
            this.numHours = new System.Windows.Forms.NumericUpDown();
            this.numMiles = new System.Windows.Forms.NumericUpDown();
            this.cbDirection = new System.Windows.Forms.ComboBox();
            this.lblMode = new System.Windows.Forms.Label();
            this.lblHours = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblDirection = new System.Windows.Forms.Label();
            this.totalsTab = new System.Windows.Forms.TabPage();
            this.tbTotalLegs = new System.Windows.Forms.TextBox();
            this.tbTotalHours = new System.Windows.Forms.TextBox();
            this.tbTotalMiles = new System.Windows.Forms.TextBox();
            this.lblTotalLegs = new System.Windows.Forms.Label();
            this.lblTotalHours = new System.Windows.Forms.Label();
            this.lblTotalMiles = new System.Windows.Forms.Label();
            this.treeView = new System.Windows.Forms.TreeView();
            this.imageListIcons = new System.Windows.Forms.ImageList(this.components);
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.menuStrip.SuspendLayout();
            this.tabControl.SuspendLayout();
            this.legTab.SuspendLayout();
            this.gbLeg.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numHours)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numMiles)).BeginInit();
            this.totalsTab.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip
            // 
            this.menuStrip.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuFile});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Size = new System.Drawing.Size(698, 33);
            this.menuStrip.TabIndex = 1;
            this.menuStrip.Text = "Menu";
            // 
            // menuFile
            // 
            this.menuFile.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.menuFile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuNew,
            this.menuExit});
            this.menuFile.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.menuFile.Name = "menuFile";
            this.menuFile.Size = new System.Drawing.Size(50, 29);
            this.menuFile.Text = "&File";
            // 
            // menuNew
            // 
            this.menuNew.Image = global::JasonGrimberg_CE06.Properties.Resources.document;
            this.menuNew.Name = "menuNew";
            this.menuNew.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.menuNew.Size = new System.Drawing.Size(210, 30);
            this.menuNew.Text = "&New";
            this.menuNew.Click += new System.EventHandler(this.menuNew_Click);
            // 
            // menuExit
            // 
            this.menuExit.Name = "menuExit";
            this.menuExit.Size = new System.Drawing.Size(210, 30);
            this.menuExit.Text = "E&xit";
            this.menuExit.Click += new System.EventHandler(this.menuExit_Click);
            // 
            // tabControl
            // 
            this.tabControl.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.tabControl.Controls.Add(this.legTab);
            this.tabControl.Controls.Add(this.totalsTab);
            this.tabControl.Location = new System.Drawing.Point(12, 40);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(304, 368);
            this.tabControl.TabIndex = 3;
            // 
            // legTab
            // 
            this.legTab.Controls.Add(this.btnAdd);
            this.legTab.Controls.Add(this.gbLeg);
            this.legTab.Location = new System.Drawing.Point(4, 29);
            this.legTab.Name = "legTab";
            this.legTab.Padding = new System.Windows.Forms.Padding(3);
            this.legTab.Size = new System.Drawing.Size(296, 335);
            this.legTab.TabIndex = 0;
            this.legTab.Text = "Leg";
            this.legTab.UseVisualStyleBackColor = true;
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(16, 278);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(260, 35);
            this.btnAdd.TabIndex = 1;
            this.btnAdd.Text = "Add";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // gbLeg
            // 
            this.gbLeg.Controls.Add(this.tbMode);
            this.gbLeg.Controls.Add(this.numHours);
            this.gbLeg.Controls.Add(this.numMiles);
            this.gbLeg.Controls.Add(this.cbDirection);
            this.gbLeg.Controls.Add(this.lblMode);
            this.gbLeg.Controls.Add(this.lblHours);
            this.gbLeg.Controls.Add(this.label2);
            this.gbLeg.Controls.Add(this.lblDirection);
            this.gbLeg.Location = new System.Drawing.Point(16, 25);
            this.gbLeg.Name = "gbLeg";
            this.gbLeg.Size = new System.Drawing.Size(240, 238);
            this.gbLeg.TabIndex = 0;
            this.gbLeg.TabStop = false;
            this.gbLeg.Text = "Leg";
            // 
            // tbMode
            // 
            this.tbMode.Location = new System.Drawing.Point(100, 157);
            this.tbMode.Name = "tbMode";
            this.tbMode.Size = new System.Drawing.Size(120, 26);
            this.tbMode.TabIndex = 4;
            // 
            // numHours
            // 
            this.numHours.DecimalPlaces = 2;
            this.numHours.Increment = new decimal(new int[] {
            25,
            0,
            0,
            131072});
            this.numHours.Location = new System.Drawing.Point(100, 114);
            this.numHours.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numHours.Name = "numHours";
            this.numHours.Size = new System.Drawing.Size(120, 26);
            this.numHours.TabIndex = 3;
            // 
            // numMiles
            // 
            this.numMiles.DecimalPlaces = 2;
            this.numMiles.Increment = new decimal(new int[] {
            25,
            0,
            0,
            131072});
            this.numMiles.Location = new System.Drawing.Point(100, 72);
            this.numMiles.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numMiles.Name = "numMiles";
            this.numMiles.Size = new System.Drawing.Size(120, 26);
            this.numMiles.TabIndex = 2;
            // 
            // cbDirection
            // 
            this.cbDirection.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbDirection.FormattingEnabled = true;
            this.cbDirection.Items.AddRange(new object[] {
            "Nowhere",
            "East",
            "West",
            "North",
            "South"});
            this.cbDirection.Location = new System.Drawing.Point(100, 29);
            this.cbDirection.Name = "cbDirection";
            this.cbDirection.Size = new System.Drawing.Size(121, 28);
            this.cbDirection.TabIndex = 1;
            // 
            // lblMode
            // 
            this.lblMode.AutoSize = true;
            this.lblMode.Location = new System.Drawing.Point(6, 160);
            this.lblMode.Name = "lblMode";
            this.lblMode.Size = new System.Drawing.Size(49, 20);
            this.lblMode.TabIndex = 0;
            this.lblMode.Text = "Mode";
            // 
            // lblHours
            // 
            this.lblHours.AutoSize = true;
            this.lblHours.Location = new System.Drawing.Point(6, 117);
            this.lblHours.Name = "lblHours";
            this.lblHours.Size = new System.Drawing.Size(52, 20);
            this.lblHours.TabIndex = 0;
            this.lblHours.Text = "Hours";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 74);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(45, 20);
            this.label2.TabIndex = 0;
            this.label2.Text = "Miles";
            // 
            // lblDirection
            // 
            this.lblDirection.AutoSize = true;
            this.lblDirection.Location = new System.Drawing.Point(6, 32);
            this.lblDirection.Name = "lblDirection";
            this.lblDirection.Size = new System.Drawing.Size(72, 20);
            this.lblDirection.TabIndex = 0;
            this.lblDirection.Text = "Direction";
            // 
            // totalsTab
            // 
            this.totalsTab.Controls.Add(this.tbTotalLegs);
            this.totalsTab.Controls.Add(this.tbTotalHours);
            this.totalsTab.Controls.Add(this.tbTotalMiles);
            this.totalsTab.Controls.Add(this.lblTotalLegs);
            this.totalsTab.Controls.Add(this.lblTotalHours);
            this.totalsTab.Controls.Add(this.lblTotalMiles);
            this.totalsTab.Location = new System.Drawing.Point(4, 29);
            this.totalsTab.Name = "totalsTab";
            this.totalsTab.Padding = new System.Windows.Forms.Padding(3);
            this.totalsTab.Size = new System.Drawing.Size(296, 335);
            this.totalsTab.TabIndex = 1;
            this.totalsTab.Text = "Totals";
            this.totalsTab.UseVisualStyleBackColor = true;
            // 
            // tbTotalLegs
            // 
            this.tbTotalLegs.Location = new System.Drawing.Point(94, 108);
            this.tbTotalLegs.Name = "tbTotalLegs";
            this.tbTotalLegs.ReadOnly = true;
            this.tbTotalLegs.Size = new System.Drawing.Size(100, 26);
            this.tbTotalLegs.TabIndex = 1;
            this.tbTotalLegs.Text = "0";
            // 
            // tbTotalHours
            // 
            this.tbTotalHours.Location = new System.Drawing.Point(94, 65);
            this.tbTotalHours.Name = "tbTotalHours";
            this.tbTotalHours.ReadOnly = true;
            this.tbTotalHours.Size = new System.Drawing.Size(100, 26);
            this.tbTotalHours.TabIndex = 1;
            this.tbTotalHours.Text = "0";
            // 
            // tbTotalMiles
            // 
            this.tbTotalMiles.Location = new System.Drawing.Point(94, 22);
            this.tbTotalMiles.Name = "tbTotalMiles";
            this.tbTotalMiles.ReadOnly = true;
            this.tbTotalMiles.Size = new System.Drawing.Size(100, 26);
            this.tbTotalMiles.TabIndex = 1;
            this.tbTotalMiles.Text = "0";
            // 
            // lblTotalLegs
            // 
            this.lblTotalLegs.AutoSize = true;
            this.lblTotalLegs.Location = new System.Drawing.Point(8, 109);
            this.lblTotalLegs.Name = "lblTotalLegs";
            this.lblTotalLegs.Size = new System.Drawing.Size(44, 20);
            this.lblTotalLegs.TabIndex = 0;
            this.lblTotalLegs.Text = "Legs";
            // 
            // lblTotalHours
            // 
            this.lblTotalHours.AutoSize = true;
            this.lblTotalHours.Location = new System.Drawing.Point(8, 68);
            this.lblTotalHours.Name = "lblTotalHours";
            this.lblTotalHours.Size = new System.Drawing.Size(52, 20);
            this.lblTotalHours.TabIndex = 0;
            this.lblTotalHours.Text = "Hours";
            // 
            // lblTotalMiles
            // 
            this.lblTotalMiles.AutoSize = true;
            this.lblTotalMiles.Location = new System.Drawing.Point(8, 25);
            this.lblTotalMiles.Name = "lblTotalMiles";
            this.lblTotalMiles.Size = new System.Drawing.Size(45, 20);
            this.lblTotalMiles.TabIndex = 0;
            this.lblTotalMiles.Text = "Miles";
            // 
            // treeView
            // 
            this.treeView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.treeView.ImageIndex = 0;
            this.treeView.ImageList = this.imageListIcons;
            this.treeView.Location = new System.Drawing.Point(322, 44);
            this.treeView.Name = "treeView";
            this.treeView.SelectedImageIndex = 0;
            this.treeView.Size = new System.Drawing.Size(364, 360);
            this.treeView.TabIndex = 4;
            // 
            // imageListIcons
            // 
            this.imageListIcons.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListIcons.ImageStream")));
            this.imageListIcons.TransparentColor = System.Drawing.Color.Transparent;
            this.imageListIcons.Images.SetKeyName(0, "upArrow.jpg");
            this.imageListIcons.Images.SetKeyName(1, "rightArrow.jpg");
            this.imageListIcons.Images.SetKeyName(2, "downArrow.jpg");
            this.imageListIcons.Images.SetKeyName(3, "leftArrow.jpg");
            this.imageListIcons.Images.SetKeyName(4, "gearIcon.png");
            this.imageListIcons.Images.SetKeyName(5, "plusSign.png");
            this.imageListIcons.Images.SetKeyName(6, "xIcon.png");
            this.imageListIcons.Images.SetKeyName(7, "document.png");
            // 
            // statusStrip
            // 
            this.statusStrip.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.statusStrip.Location = new System.Drawing.Point(0, 426);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.Padding = new System.Windows.Forms.Padding(2, 0, 14, 0);
            this.statusStrip.Size = new System.Drawing.Size(698, 22);
            this.statusStrip.TabIndex = 5;
            this.statusStrip.Text = "Status";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ClientSize = new System.Drawing.Size(698, 448);
            this.Controls.Add(this.statusStrip);
            this.Controls.Add(this.treeView);
            this.Controls.Add(this.tabControl);
            this.Controls.Add(this.menuStrip);
            this.Name = "MainForm";
            this.ShowIcon = false;
            this.Text = "Travel Planner";
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.tabControl.ResumeLayout(false);
            this.legTab.ResumeLayout(false);
            this.gbLeg.ResumeLayout(false);
            this.gbLeg.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numHours)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numMiles)).EndInit();
            this.totalsTab.ResumeLayout(false);
            this.totalsTab.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.ToolStripMenuItem menuFile;
        private System.Windows.Forms.ToolStripMenuItem menuNew;
        private System.Windows.Forms.ToolStripMenuItem menuExit;
        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage legTab;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.GroupBox gbLeg;
        private System.Windows.Forms.TextBox tbMode;
        private System.Windows.Forms.NumericUpDown numHours;
        private System.Windows.Forms.NumericUpDown numMiles;
        private System.Windows.Forms.ComboBox cbDirection;
        private System.Windows.Forms.Label lblMode;
        private System.Windows.Forms.Label lblHours;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblDirection;
        private System.Windows.Forms.TabPage totalsTab;
        private System.Windows.Forms.TextBox tbTotalLegs;
        private System.Windows.Forms.TextBox tbTotalHours;
        private System.Windows.Forms.TextBox tbTotalMiles;
        private System.Windows.Forms.Label lblTotalLegs;
        private System.Windows.Forms.Label lblTotalHours;
        private System.Windows.Forms.Label lblTotalMiles;
        private System.Windows.Forms.TreeView treeView;
        private System.Windows.Forms.ImageList imageListIcons;
        private System.Windows.Forms.StatusStrip statusStrip;
    }
}

