﻿using System;
using System.Windows.Forms;

/// <summary>
/// Jason Grimberg
/// CE06 TreeView and TabControl
/// Location Travel Planner form that will list out directions
/// in a tree-view and total them up in a separate tab
/// </summary>
/// 
namespace JasonGrimberg_CE06
{
    public partial class MainForm : Form
    {
        // New data constructors
        public Data NewData
        {
            // Getters for new data
            get
            {
                // New data set and return text
                Data d = new Data();
                d.Direction = cbDirection.Text;
                d.Miles = numMiles.Value;
                d.Hours = numHours.Value;
                d.Mode = tbMode.Text;
                return d;
            }
            // Setters for the new data
            set
            {
                // Setting the data to it's values
                cbDirection.Text = value.Direction;
                numMiles.Value = value.Miles;
                numHours.Value = value.Hours;
                tbMode.Text = value.Mode;
            }
        }

        // Initialization of main form
        public MainForm()
        {
            InitializeComponent();

            // Setting the index of the direction to zero
            cbDirection.SelectedIndex = 0;
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            // New data call
            Data d = new Data();

            // Setting the data fields to the data class
            d.Direction = cbDirection.Text;
            d.Miles = numMiles.Value;
            d.Hours = numHours.Value;
            d.Mode = tbMode.Text;

            // Setting the current text boxes to add
            string milesToAdd = tbTotalMiles.Text;
            string hoursToAdd = tbTotalHours.Text;
            string legsToAdd = tbTotalLegs.Text;

            // If statement to see if East is selected
            if (cbDirection.Text == "East")
            {
                // Building the tree with arguments
                BuildTree(d.Direction, d.Miles, d.Hours, d.Mode, 1);

                // Adding the totals with the current arguments
                Totals(milesToAdd, d.Miles, hoursToAdd, d.Hours, legsToAdd, 1);
            }
            // If statement to see if North is selected
            else if (cbDirection.Text == "North")
            {
                // Building the tree with arguments
                BuildTree(d.Direction, d.Miles, d.Hours, d.Mode, 0);

                // Adding the totals with the current arguments
                Totals(milesToAdd, d.Miles, hoursToAdd, d.Hours, legsToAdd, 1);
            }
            // If statement to see if South is selected
            else if (cbDirection.Text == "South")
            {
                // Building the tree with arguments
                BuildTree(d.Direction, d.Miles, d.Hours, d.Mode, 2);

                // Adding the totals with the current arguments
                Totals(milesToAdd, d.Miles, hoursToAdd, d.Hours, legsToAdd, 1);
            }
            // If statement to see if West is selected
            else if (cbDirection.Text == "West")
            {
                // Building the tree with arguments
                BuildTree(d.Direction, d.Miles, d.Hours, d.Mode, 3);

                // Adding the totals with the current arguments
                Totals(milesToAdd, d.Miles, hoursToAdd, d.Hours, legsToAdd, 1);
            }
            // If statement to see if Nowhere is selected
            else if (cbDirection.Text == "Nowhere")
            {
                // If nowhere is selected when the add button is pressed
                // a pop up window will prompt you to add a direction
                MessageBox.Show("Please choose a direction.", "Oops!",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void menuNew_Click(object sender, EventArgs e)
        {
            // Clears all of the treeView nodes
            treeView.Nodes.Clear();

            // Sets new data to clear out all fields
            NewData = new Data();

            // Setting the index of the direction back to zero
            cbDirection.SelectedIndex = 0;

            // Setting the first tab as default
            tabControl.SelectTab(0);

            // Clearing the total text boxes
            tbTotalHours.Text = "0";
            tbTotalMiles.Text = "0";
            tbTotalLegs.Text = "0";
        }

        private void menuExit_Click(object sender, EventArgs e)
        {
            // Closes the application
            Application.Exit();
        }

        // Build tree method
        private void BuildTree(string nodeName, decimal nodeMiles,
            decimal nodeHours, string nodeMode, int imageIndexNum)
        {
            // New tree node
            TreeNode node = new TreeNode();

            /*
             * Adding to each node
             * Choosing an image index for the root node
             * Choosing the selected image index
             * Adding sub node for miles, hours, and mode
             * and setting an image for each with a 
             * selected image index as well
             */
            node.Text = nodeName.ToString();
            node.ImageIndex = imageIndexNum;
            node.SelectedImageIndex = 4;
            node.Nodes.Add($"{nodeName}", $"Miles: {nodeMiles}", 7, 4);
            node.Nodes.Add($"{nodeName}", $"Hours: {nodeHours}", 5, 4);
            node.Nodes.Add($"{nodeName}", $"Mode: {nodeMode}", 6, 4);

            // Adding all that data to a new node
            treeView.Nodes.Add(node);
        }

        // Totals method
        private void Totals(string oMile, decimal nMile, string oHours,
            decimal nHours, string oLegs, int nLegs)
        {
            // New variables to hold data
            decimal newMiles;
            decimal newHours;
            int newLegs;

            // Pulling the decimal from miles text
            newMiles = nMile + decimal.Parse(oMile);

            // Pulling the decimal from hours text
            newHours = nHours + decimal.Parse(oHours);

            // Pulling the decimal from legs text
            newLegs = nLegs + Int32.Parse(oLegs);

            // Putting the added decimals into the text boxes
            tbTotalMiles.Text = newMiles.ToString();
            tbTotalHours.Text = newHours.ToString();
            tbTotalLegs.Text = newLegs.ToString();

        }

    }
}
